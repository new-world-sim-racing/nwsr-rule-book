# /bin/bash
cd rulebook
rm -rf $TARGET_FILE
pandoc _pdf_metadata.md \
    content/changelog.md \
    content/community_regulations.md \
    content/schedule.md \
    content/standards.md \
    content/stewarding.md \
    --listings \
    --template ./templates/eisvogel.tex \
    -H ./includes/pandoc/chapter_break.tex \
    -H ./includes/pandoc/code_break.tex \
    -o ${TARGET_FILE}
