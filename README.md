# New World Sim Racing Rule Book

[![Read Rulebook online](https://img.shields.io/badge/Read_Rulebook_Online-blue.svg)](https://new-world-sim-racing.gitlab.io/nwsr-rule-book/)
[![Download Rulebook as PDF](https://img.shields.io/badge/Download_Rulebook_as_PDF-blue.svg?logo=adobeacrobatreader)](https://new-world-sim-racing.gitlab.io/nwsr-rule-book/downloads/NWSR-RuleBook.pdf)

[![pipeline status](https://gitlab.com/new-world-sim-racing/nwsr-rule-book/badges/master/pipeline.svg)](https://gitlab.com/new-world-sim-racing/nwsr-rule-book/-/pipelines?page=1&scope=all&ref=master)
[![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org/)
[![powered by Docsify](https://img.shields.io/badge/powered_by-docsify-blue.svg)](https://docsify.js.org/)
[![Discord](https://img.shields.io/discord/795239360323780629.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.io/newworldsimracing)

![New World Sim Racing logo](./rulebook/images/logo.png)

Welcome to the NWSR Rule Book repository.

This repository contains the documentation used to generate our driver's rule book.

The book is generated automatically for every commit pushed that changes contents of the rulebook folder. When updates to the rule book are ready, it is deployed into our `#rulebook` channel on our Discord server, so keep an eye out for that.

## Contributing to the rulebook

Our driver's rule book is an open source project. We'd love to see the community contributing if you'd like to. Please follow these simple guidelines below to contribute to the project.

### Reporting issues

The simplest way to report mistakes, issues or suggestions is to directly create entries in the [issues page](https://gitlab.com/new-world-sim-racing/nwsr-rule-book/-/issues) here on gitlab. Someone will pick them up and work on them eventually.

### Going further - Updating documentation

To directly contribute to the rule book's content, please keep in mind the following:

* Create a new branch off of the main `master` branch.
* Name your branch with a brief and clear name indicating what changes you're looking to make.
* Make sure you add an entry under the [changelog page](./rulebook/content/changelog.md) with a short brief on what changes you've added. If you do not do this, you'll be asked to add one when you request to merge your changes.
* Have a look at the [pipeline logs](https://gitlab.com/new-world-sim-racing/nwsr-rule-book/-/pipelines) for your pushed commits to make sure your edits do not contain any markdown issues or spelling mistakes. If there are any words that need to be added to the rule book's dictionary, add them alphabetically in the [spellings file](./rulebook/.spelling).
* Once you have completed your changes, [create a merge request](https://gitlab.com/new-world-sim-racing/nwsr-rule-book/-/merge_requests). Your changes will be reviewed then merged, then will be published directly to our discord.

### Adding a new chapter

If a new chapter is to be added to the rulebook for any reason, a new markdown file should be generated within the [rulebook](./rulebook/content) folder with a short and consice name, and an entry to the chapter should be added into the following files in the required order:

* [generate-pdf.sh](generate-pdf.sh) for the PDF generator
* [rulebook/_sidebar.md](rulebook/_sidebar.md) for the web generator

## Running markdown tests locally

If you would like to run all the tests locally before pushing your content updates to the repository, you can use Docker to execute them the exact same way they would be executed in the CI pipeline. Open the terminal in this directory and run:

```bash
docker-compose up markdown-lint markdown-spell-check markdown-grammar-check
```

All the containers should start and exit with code 0 indicating the tests have passed. If there are any errors, please correct them, then push your code changes.

## Generating the rulebook PDF locally

You will need Docker and docker-compose for this. Open a terminal in this directory and run:

```bash
docker-compose up pandoc
```

A copy of the rulebook will be generated into `./volumes/output/NWSR-RuleBook.pdf` which is identical to the PDF the CI/CD pipeline would generate.

If any errors occur, double check that the pandoc configuration has not been tampered.

## Running the rulebook website locally

Similar to the PDF, open a terminal in this directory and run:

```bash
docker-compose up docsify
```

You'll be able to view the generated rulebook site at <http://localhost:3000/>

## Community

* [Discord server](https://discord.io/newworldsimracing)
* [New World Sim Racing website](https://newworldsimracing.com/)
* [The SimGrid community page](https://www.thesimgrid.com/hosts/newworldsimracing)
* [Simracing.GP community page](https://beta.simracing.gp/communities/ikG1uiyY6vvTGCTAL486M)
