# New World Sim Racing Rule Book

[![powered by Docsify](https://img.shields.io/badge/powered_by-docsify-blue.svg)](https://docsify.js.org/)

Welcome to the online version of the New World Sim Racing Rule Book.

The same content of this rulebook can be downloaded in PDF form if you'd rather read it offline

[![Download as PDF](https://img.shields.io/badge/Download_as_PDF-blue.svg?logo=adobeacrobatreader)](downloads/NWSR-RuleBook.pdf ':ignore')

## Community {docsify-ignore}

[![Discord](https://img.shields.io/discord/795239360323780629.svg?&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.io/newworldsimracing) [![New World Sim Racing website](https://img.shields.io/badge/NWSR_Website-blue.svg?logo=)](https://newworldsimracing.com/) [![The SimGrid community page](https://img.shields.io/badge/The_SimGrid_Community-blue.svg?logo=)](https://www.thesimgrid.com/hosts/newworldsimracing) [![Simracing.GP community page](https://img.shields.io/badge/Simracing.GP_Community-blue.svg?logo=)](https://beta.simracing.gp/communities/ikG1uiyY6vvTGCTAL486M)

## Contributing to the rulebook {docsify-ignore}

Our driver's rule book is an open source project. We'd love to see the community contributing if you'd like to. Please follow these simple guidelines below to contribute to the project.

Instructions on how to contribute are listed within the Gitlab repository below.

[![Gitlab](https://img.shields.io/badge/Contribute_on_Gitlab-FB6E26.svg?logo=gitlab)](https://gitlab.com/new-world-sim-racing/nwsr-rule-book)
