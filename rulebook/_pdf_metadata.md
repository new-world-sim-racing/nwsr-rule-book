---
# title: "New World Sim Racing"
# subtitle: "Driver's Rule Book"
geometry: margin=1in
toc: yes
subparagraph: yes
titlepage: true
titlepage-color: "1a1c2d"
titlepage-text-color: "ffffff"
titlepage-background: "images/title-page-background-new.jpg"
# abstract: |
#     New World Sim Racing is a group geared for the non aliens of North and South America. Our main focus started out with ACC/AC, we've been slowly branching into rF2, and AMS2 is in the near future. Starting out as an Americas branch of ACCSS (The ban hammer group), we still hold the idea that we're geared for amatures with our fastest drivers being more than two seconds off of alien pace.
---

