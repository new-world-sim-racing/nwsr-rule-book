# Community Schedule

Most events are run through the [The SimGrid](https://www.thesimgrid.com/) platform. Currently supported games on the platform are Assetto Corsa, Assetto Corsa Competizione and iRacing, with Automobilista 2 and RaceRoom coming soon.

## Assetto Corsa Competizione

### GT3 series

GT3 series events occur every Sunday at 5pm PST / 8pm EST for the duration of the championship, with a 2-4 weeks break in between seasons.

The events in this series consist of 1h long GT3 races with a single mandatory pitstop with a minimum stop time of 25 seconds and optional tire swaps. Occasionally some of the events might have alternative configurations, and these will be announced at least a week ahead of time.

At the event date, the race server will be started an hour before the scheduled event time. Drivers will be given a 1 hour Practice 1 session which is intended to allow drivers to begin joining the server at their leisure leading up to the event start.

At the scheduled event time 15 minutes are given for Practice 2, followed by 31 minutes of Split Qualification sessions, then a 1h race. During split Qualification, splits will each have 10 minutes to complete their qualification laps in this order: `Bronze` > `Silver` > `Gold`. The first split will get an extra minute to compensate for adjusting their setups. Split qualification rules can be viewed in the **Driving Standards** section.

Drivers will only score points for races in this series if they complete a minimum of 80% of the race distance.

Drivers will also be allowed to request to change vehicles before the start of the third race of the season. Any later requests will be allowed but the driver will receive a 20 point reduction in standings points for doing so.

These events are broadcast on our NWSR Twitch channel, and all contacts and reports are reviewed a few days post-race for stewarding. Stewarding details for this series can be viewed in the **Stewarding** section.

### M-Tuesdays

M-Tuesdays is the reincarnation of M2 Mondays, one of our popular laid back M2 series. M-Tuesdays is a short championship scheduled to run on Tuesdays at 5pm PST / 8pm EST.

The events in this series consist of two 25 minute BMW M2 races with no mandatory pitstop and no pit-window. Race 1 will always be in daylight, while Race 2 will be at dusk.

At the event date, the race server will be started at the posted event time. Drivers will be given a 30 minute Practice 1 session which is intended to allow drivers to begin joining the server at their leisure leading up to the event start.

30 minutes past the scheduled event time, 15 minutes are given for Practice 2, followed by 20 minutes for Qualification, then the two 25 minute races. There is no split Qualification for this series.

These events are broadcast on our NWSR Twitch channel and there is no associated stewarding. Unacceptable behavior will however result in the driver being removed from the series.

### Endurance series

Endurance series events occur at most once a month on the Pint series off-weeks at 2pm PST / 5pm EST.

The events in this series consist of 2.4h long GT3 races with two mandatory pitstops for tires. You will be expected to devise and manage a strategy for these races.

At the event date, drivers will be given 15 minutes of Practice, followed by 20 minutes of Split Qualification (GT3/GT4), then a 2.4h race.

### Community races

All the events under this category consist of more relaxed and for fun community races. These events may change or be retired based on community interest and participation.

#### Multiclass Mondays

Multiclass Mondays events usually occur on Mondays between 8 and 9pm EST. These events could consist of 2x 25 minutes or 30 minutes sprint or 1 hour Multiclass races. The second race is usually held at night time, and as a Reverse Grid race based off of the qualification placement of the drivers.

#### Random Randomness

At random, any of our community events could be marked as a Random Randomness event. This means every car assignment, track and weather will be randomized 5 minutes before the event time. You may sign up to the event with any vehicle as a placeholder.

#### Experimental Events

We occasionally run events with either tricky weather configurations or with a limited car selection. A few variations of this format we've done in the past as examples and can bring back are:

* Thunderdome at Bathurst: 2x 25 minute races in extreme rain and darkness at Bathurst
* Jag Only 2x 25 minute races at Monza, with a reverse grid second race
* Misano Sotto le Stelle in Inverno: hour long race at Misano with freezing temperatures
* Furnace at Bathurst: Hour long race with extremely high temperatures at Bathurst

We will be continuing to run these every few Mondays during community races.

## iRacing

### LMP3 Endurance Championship

LMP3 Endurance Championship events occur on up to 3 times a month on Saturdays at 5pm PST / 8pm EST.

The events in this series consist of 1.5h and 2h long LMP3 Endurance races with Fixed Setups, Full Course Cautions and 3 Fast Repairs, unless otherwise specified.

At the event date, the race server will be started at the scheduled time. Drivers will have 45 minutes for Practice, followed by a Solo Qualifying session, a Driver Briefing, then the race.

The championship scoring is maintained outside of The SimGrid. More details available in the relevant Discord channels. The scoring is distributed for driver **teams** of up to 3 drivers, in separate cars. Any driver without a team will be paired up during the championship. Teammates are **required** to run matching liveries for these events. Additionally, a points percentage boost is applied to drivers of lower skill splits to allow for more competition.

Drivers must be 3000 iRating or Lower, Unless Permitted as Special Entry. Special exemptions are long time NWSR community members or Special Invitees.

These events are stewarded post-race and broadcasts are to be determined at this moment. This championship features prizes for the top 3 individuals in Driver Points, and for the top 3 teams in Team Points.

<!-- End of section -->
