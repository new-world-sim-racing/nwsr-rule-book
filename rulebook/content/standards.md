# Driving Standards

## Procedures

### Full formation lap

When running events configured with a full formation lap, there is no position widget to guide the lap. All ACC events run in this league are using this configuration. Other sims may offer different formation lap procedures, in which case the procedure will be communicated during the Driver Briefing.

We request all drivers to avoid driving into each other especially at the start of the formation lap, even though all cars are ghosted. Any drivers found intentionally driving into others during the formation lap will be penalized.

The race leader is to set a comfortable pace of 120 km/h for the single file phase of the formation lap to allow all drivers to warm their tires and brakes.

Once double file is called, the leader must maintain 80 km/h and all drivers are to reduce the gaps to the cars in front of them to under 2 car lengths as early as possible to make sure all drivers throughout the grid are also ready for the Green lights.

Drivers found jumping the start or being out of position will be heavily penalized, which may include a qualification ban and/or points reduction. Drivers leaving a larger gap than 2 car lengths might also receive a penalty at the discretion of the stewards.

### Race starts

All drivers are expected to take extra care on race starts. We've observed over the last few seasons that most of the incidents happen in the first three laps of races. All drivers should be aware that applied penalties are doubled for lap 1 incidents. While we encourage competitive and hard racing, we expect all drivers to race clean. This means we expect all drivers to adhere to the following points, and especially so during the first few laps of a race.

* When starting a race, expect a very large slowdown zone going into corners when behind other cars, which can extend to over 100 meters earlier than the hotlapping braking point the farther back in the field you are.
* We expect all drivers to know to be ready to slowdown early during the first few laps where cars are packed tight.
* When a driver is behind two or three cars battling for position that are preparing to take a corner two or three wide, the trailing driver should expect the leading drivers to brake earlier and longer to be able to successfully make the corner. It is your responsibility as the trailing car to take the necessary precautions to avoid causing an accident.
* Divebombs leading into corners often lead to accidents if the leading car does not expect it and takes their legal racing line. Drivers are expected to avoid divebombs as much as possible, especially on the first few race laps. If for some reason the trailing driver misses their braking point and sends themselves into the corner apex, they are expected to hand the position back in the next few corners for the best sportsmanship.
* Leaving too large of a gap leading into the green flag causes all the drivers behind you to be out of position. If the gap is significant, you may be penalized at the discretion of the stewards.
* Being caught out of position on a race start will result in a penalty being handed to the driver. This does not apply to drivers moved farther back due to a leading driver leaving a significant gap.
* Drivers caught jump starting a race start will receive a penalty. If you find yourself having to accelerate out of a corner right before the lights go green, you are expected to ease off and keep your position until after the start line. Passing a car that has entirely missed their launch does not count as jumping the start.
* GT3 series race starts are post-race stewarded. Penalties will be handed out to drivers that cause collisions

Respecting these rules will allow all drivers to start their races as cleanly as possible and should make for good races. Races are not won on turn or lap 1, but they can absolutely be ruined then and there.

### Qualification

* During qualification, all drivers are expected to adhere to all racing standards listed in this section.
* Drivers are expected to leave enough room in front of them when following a slower driver **before** reaching the final corner of the track.
* To avoid a traffic jam right before the end of the outlap, please consider staggering your exits. You will have more than enough time to complete multiple laps for your qualification.
* You are not allowed to pass a driver on a flying lap, even if you are quicker than them unless the driver in front clearly yields his place. You may communicate with the driver in front that you wish to pass on Discord in the event voice channel if both of you are connected. Otherwise, you are expected to back off and create enough room for yourself to complete your laps.
* If you invalidate your lap, you are expected to yield to any faster car on a flying lap. The game should automatically wave you a Blue flag in this case, and you are expected to follow it.
* You are not permitted to impede other drivers' qualification laps otherwise you will be issued a penalty or disqualified from the race at the discretion of the stewards.
* You should *not* stop in the middle of the road to teleport back to the pits. Please move out of the road and teleport in a safe place.

### Split Qualification

When an event follows our split qualification configuration, drivers will be expected to respect the following rules or risk qualification disqualification, forcing to start at the back of the grid come race time. Assigned split times will be communicated prior to or at the beginning of the qualification session.

* All qualification rules apply for this session type.
* If it is understood that it is a `Pro` split only time, no drivers other than those listed under the `Pro` category are allowed on track.
* If a split is deemed to be allowed to run until the `10 minutes` mark for example, they are allowed to complete a flying lap only if they cross the start/finish line before the timer hits 10 minutes. If it is crossed only a few seconds before the given time, they are allowed to complete the lap and then are expected to return to pits immediately after.
* Drivers from other splits are allowed to line up before the pit exit line (delimited by the speed limit sign), and are only allowed out after the timer hits their designated period.
* Qualification time could be different for each split. It is determined by the event organizers beforehand and will be communicated to all drivers at or before the qualification session.
* A driver out on track when it is not their split's turn will receive a qualification disqualification and will start from the back of the grid during the race.
* Exceptions may be granted by event organizers if there is a mistake. These will be clearly communicated in the in-game chat to the affected drivers.

## Racing rules

### Yellow and Double-Yellow flags

* When a yellow flag is waived at you, you must exercise caution going into the next few corners.
* Drivers must be cautious and prepare to slow down and avoid any incident ahead. Drivers are expected to slow down enough to avoid coming into contact with spun, slow or slowing cars that might end up on the race track.
* Drivers are strictly not allowed to pass during a yellow flag condition.
* Ignoring yellow flags or passing under yellow flags may result in a penalty at the discretion of the stewards during or after the race.
* Causing an incident while a yellow or double yellow flag is waived will be considered for heavier penalties during incident reviews.

Yellow flag rules will be strictly enforced.

### Blue flag

* A driver being shown a blue flag is not allowed to defend his track position against the lapping car.
* Drivers about to be lapped should be predictable in their actions and not make sudden changes of direction or track position. Do not brake in front of the lapping car, and do not lift or brake in the middle of a corner exit. Always allow lapping cars to pass on a straight, or move off the racing line and exercise caution when in a twisty or technical section of the track.
* Intentionally holding up or defending against a lapping car may result in penalties or even disqualifications being applied during or after the race.
* The driver who is in the lapping car and approaching a blue flagged car must still adhere to track rules and make a pass safely, in a place that is also safe to do so. If you are the lapping vehicle, you should also expect the lapped car to have earlier braking points than yourself. Be prepared to brake much earlier than you usually do.
* Lapped drivers are allowed to unlap themselves, but they should request to do so to the car ahead over voice comms, or make it very clear that it is their intention to pass. If you are allowed to pass, you must be able to pull away after passing and not hold up the driver.

### Passing and on-track behavior

* During race starts, attempting to begin out of position is not allowed. If a driver would really like to allow the field to head forward, they should simply not select Drive during the countdown. Joining the grid and slowing down to allow other drivers in front will be heavily penalized if not disqualified.
* Overtakes out of the race track limits are illegal and will be penalized.
* When rejoining the track after going off, ensure you make the rejoin as predictable as possible for trailing cars, and make your rejoin angle as parallel as possible to the race track. Bad rejoins can and will cause major incidents, and will be heavily penalized.
* When battling for position, the driver in front has the right to choose any line at any section of the track. The driver in front loses this right when an overtaking driver brings their front wheel to line up with the rear wheels of the other driver's car. At this point drivers are side-by-side and should both give each other at least 1 car's width room.
* If the driver in front overshoots his braking point and loses the racing line, a closely following driver is allowed to set himself onto the racing line and if both drivers end up side-by-side in the corner as a result then both must give each other enough room at corner exit.
* Defending is allowed and accepted as one reactionary move by the driver in front. They are not allowed to defend if there is any overlap between cars. Note that following a racing line into a corner is **not** considered defending, unless both cars are already side-by-side.
* Both the passing driver and the driver in front are responsible for fair racing during the pass. It is the passing driver's responsibility to choose a safe time for the pass.
* Passing attempts when going into a turn are where most accidents happen. The preferred racing line through most corners is usually very narrow and sometimes it will be impossible for two cars to negotiate a tight corner side-by-side at full speed. For each corner, the right to the preferred racing line is decided at the turn-in point. A driver attempting to pass at corner entry has to be in a side-by-side position at the turn-in point to have a right to enter the corner side-by-side. If not, they must back off and give way to the driver up front.
* Dive-bombing should be avoided and is subject to a penalty at the discretion of the stewards. If the attempting driver was out of control and causes contact or time loss to another driver, penalties will be applied during or after the race.
* Brake checking, unnecessary slowing through a corner, punting, bump-passing, cutting-off, and chopping are all deliberate actions. These actions are not permitted whether there is contact or not. They will be subject to a penalty.
* Blocking is not allowed and is subject to a penalty.
* Weaving to break a draft is considered blocking. Only one reactionary move is allowed between corners. Weaving multiple times on straights is punishable (observed a lot on the Paul Ricard Mistral straight for example).
* Out of control or spinning drivers should lock their brakes to make it easier for other drivers to predict their movements. After a spin, the driver must keep the brakes pressed to make sure the car does not roll forward or back to avoid creating an unpredictable situation for cars passing by.
* Drivers who go off track limits but can keep their cars under control should slow down or wait for traffic to clear and rejoin in a safe manner.
* Cutting the track to gain an advantage and/or pass another driver is not allowed and will be subject to a penalty. You must yield back the position if you gained it through a track cut.
* Cutting the track on the final corner to gain an advantage during qualification is strictly not allowed and will be subject to a penalty.
* Pitlane entry and exit lines are to be respected. Crossing the pitlane exit line will result in a penalty, even if the game does not hand it out automatically. This rule will be strictly enforced.
* The last point also applies to vehicles not coming out of the pitlane, when the racing line crosses over the line. We have seen accidents where a vehicle has been struck coming out of the pits due to the trailing car crossing over preparing for the following corner. It is the responsibility of the driver on the track to allow ample room for drivers coming out of the pits. The game already warns about this and we expect drivers to exercise caution.
* Intentional collisions are absolutely prohibited. Keep in mind other drivers may be running high torque wheels which can very easily lead to physical injuries.
* Please avoid venting in frustration in the event voice chats in case of incidents. We understand that being involved in an incident is frustrating whether it is your fault or not. All incidents are recorded, reviewed, and will be penalized accordingly. If not during the race, then post-race penalties will still apply to the driver at fault.
* When a large collision occurs during the first 3 laps of the race, it will be up to the broadcast or admin team to quickly review and decide if the session is to be restarted. Only if a significant portion of the field is involved in the incident, the session will be restarted. Note that all incidents are still recorded and **will** be reviewed for the first session. We also recommend drivers save their setups during the first preparation phase to avoid losing changes in the event of a restart.
* In the event of a red-flag restart, a message will be posted in the game chat and will be announced in the voice channels prior to resetting.

<!-- End of section -->
