# Community Regulations & Guidelines

This section is intended to provide a guide for NWSR community rules and policies on and off the track.

## Discord

Our discord community is available to all to join at the following link: [https://discord.io/newworldsimracing](https://discord.io/newworldsimracing)

Participating in our racing events requires you to have access to [our racing community on the The SimGrid](https://www.thesimgrid.com/hosts/newworldsimracing) (TSG) platform. You may sign up to league events by following the community on TSG and joining our Discord, then signing up for the events listed on TSG's website.

**NWSR requires you use your real name within our discord server.** Make sure you have already set your discord nickname on the server to your real name. Failing to do so means you may be removed from our events.

The following behavior is expected of you on our Discord server:

* Please be polite and respectful to each member when interacting on the discord server.
* No personal attacks on other users or staff members directly on the server or in private messages.
* Any impersonation of a user in any mode of communication is strictly prohibited and will result in a ban.
* Illegal software or content violating the Discord terms and conditions should never be shared.
* Images may be posted as long as they are not explicit or offensive.
* We reserve the right to remove any topics we deem inappropriate or disruptive to our community.
* Direct your posts to the correct channels to avoid confusion on the server.
* If you feel like a staff member has made a decision you deem wrong, please take the time to direct your concern to them via private message.

We reserve the right to ban any member that violates our guidelines or disrupts our community. We will be fair and provide warning in most cases.

## The SimGrid

The SimGrid is an online multiplayer platform that has been designed to offer sim racing enthusiasts a central place to host their online racing community across a variety of racing simulations. More information on the platform is available here: [https://www.thesimgrid.com/](https://www.thesimgrid.com/)

NWSR uses TSG to host racing events for Assetto Corsa Competizione and iRacing. All community events are created and open for registration on the platform at least a few days before their time. Feel free to sign up to any event you might be interested in that matches you availability. All of our official events are listed weeks in advance on TSG and can be closed for registrations during the length of the championships that they constitute. More information is available for each series in the [racing schedule](content/schedule.md#community-schedule) section.

## Streaming regulations

There are no restrictions on streaming your races online.

<!-- End of section -->
