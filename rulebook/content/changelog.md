# Change log

| Date | Author | Comment |
|----------|---------------|:--------------------------------------------------------|
|2021-03-13|Fares Saidi|Driver's Rule Book created|
|2021-03-15|Fares Saidi|Split Qualification rules updated for the scheduled Pint series|
|2021-08-25|Nicolas Alvarado|Registration process updated, removed NWSR website guidelines and updated Schedule & Driving Standards|
|2021-08-28|Fares Saidi|Added **Race starts** section and updated **Passing and on-track behavior** section in the driving standards chapter|
|2021-09-30|Fares Saidi|Updated Blue Flag rules under the **Driving Standards** chapter and corrected the **Community Schedule**|
|2021-12-05|Fares Saidi|Added **Incidents and Penalties** and **License black points** sections and updated **Full formation lap** procedure, **Race starts**, **Penalty protests**, and **Passing and on-track behavior** under the **Driving Standards** chapter|
|2022-03-12|Fares Saidi|Updated **Incidents and Penalties** to include more variants of incidents we've seen in previous seasons, added a note on when live penalties are applied during races and added a minimum race distance note & vehicle swap under the Pint season schedule|
|2023-01-08|Fares Saidi|Updated **Incidents and Penalties** to include more variants of incidents we've seen in previous seasons. Updated notes for the split qualification procedures section. Added points for driver behavior about post-race collisions and voice chat venting. Updated platform to TSG under community regulations. Corrected schedule with more up to date details for the GT3 series|
|2023-01-28|Fares Saidi|Updated definition of "side-by-side" in the **Driving Standards** chapter|
|2023-07-22|Fares Saidi|Adjusted timing for split-qualification for the first-running split in the **Community Schedule** chapter. Added penalty for starting out of position and added related rule. Added details for red-flag restarts, and updated wording around pitlane lines and yellow flags in the **Driving Standards** chapter.|
|2023-09-25|Fares Saidi|Moved stewarding details to its own **Stewarding** chapter. Updated **Community Schedule** chapter to add ACC M-Tuesdays Series and iRacing LMP3 Endurance Championship.|
__________________________

<!-- End of section -->
