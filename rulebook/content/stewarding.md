# Stewarding

## ACC GT3 Series

### Incidents and Penalties

It should be known that we make use of software that reports all collisions, whether light or heavy. For events that are live stewarded, as many of these incidents are reviewed directly during the race and appropriate penalties are applied as necessary.

Historically 80% of all our recorded incidents happen during the first two laps of the race. Our penalties are harsher for lap 1 incidents to promote cleaner starts.
**Lap 1 penalties are always doubled both for time penalties and license points**

Race-live penalties and license points for guilty drivers are as follows:

| Penalty description | Time Penalty | License Points Penalty |
| ----------------------------------- | --------------- | --------------- |
| Warning | No time penalty | 1 license point per 2 warnings |
| Gaining an advantage by going off track | 5 second time penalty | 3 license points |
| Collision with no positions lost | 5 second time penalty | 3 license points |
| Collision with positions lost | 10 or 15 seconds (Drive thru for lap 1) | 4 or 6 license points |
| Collision with multiple vehicles | 15 or 30 seconds (30 seconds or Stop & Go 10 seconds for lap 1) | 9 or 15  license points |W
| Unsafe rejoin | 5 seconds | 2 license points |
| Blue flag warning | No time penalty | 1 license point per 2 warnings |
| Blue flag penalty | 5 seconds | 2 license points |
| Moving under braking | 5 seconds | 2 license points |
| Moving under braking and causing a collision | 10 seconds | 4 license points |
| Avoidable contact | 5 seconds | 2 license points
| Avoidable collision under yellow conditions | 10 seconds | 5 license points |
| Dangerous driving | 5 to 10 second time penalty | 3 to 6 license points |
| Starting out of position | 30 seconds to Drive Through | 6 to 9 license points |
| Unacceptable behavior | 60 seconds, Stop & Go 30 or instant disqualification (Possible disqualification from  rest of the series) | 15 or 30 license points |

Drivers should be aware that application of most time penalties in game will be handed to driver **AFTER** they complete their mandatory pitstop, so that we cancel out misjudged penalties in case of a successful protest, without affecting final track position. Drive Thru and Stop & Go penalties however will **NOT** be left until after a pit stop and will be instead applied as soon as the incidents are reviewed and driver has been found guilty.

### License points

As mentioned in the last section, license points are directly linked to incidents drivers are found guilty of.

Reaching the following license points amounts will result in further penalties:

* 30 points: Qualification ban in the next race
* 50 points: Race disqualification for the next race event
* 70 points: Disqualification from the season

### Post-Race penalty reviews

In case the event is not live-stewarded, or we are not able to tend to all incidents during the race, then the recorded & reported incidents are reviewed by a full team of stewards and penalty results are agreed upon by the team on the Tuesday following the race. Penalty results are then posted in the relevant **#[series-X]-penalties** channel in our Discord.

If you find yourself victim to an unsafe rejoin by another driver heavily impeding your lap or causing you lose control and veer off track, please try to remember the lap number and report it manually using the series' protest form.

In case of post-race penalties, we deduct the same amount of points from your driver standings as are given license points. Details for the license points are under the [Incidents and Penalties](#incidents-and-penalties) section

### Penalty protests

Drivers may protest given penalties or missed incidents by using the relevant protest form link for the series. Drivers will have until the Thursday following the race to submit them for post-race review by the stewarding team. Dates are subject to change and will be communicated in the Discord announcements.

Due to the way The SimGrid handles publishing results, final results for a completed race will only be posted after the protest period has elapsed. Provisional standings will automatically be displayed on the driver standings page, but race results will not be viewable until then.

Drivers will only have **2** possible protest attempts within the season. However if a protest is deemed accepted, the protest attempt will be refunded.

Reporting missed incidents or another car for rejoining the track dangerously does **NOT** count as a protest attempt, so we encourage drivers to do so.

When submitting a protest in the protest form, you'll be asked to fill in answers to all of these otherwise you won't be able to submit.

```txt
* Race #: 1
* Your Car #: 1
* Opponent car(s) #: 2
* Lap #: 1
* Turn #: 1
* Short description of the incident: Unsafe re-joining result in my car being hit and spun.
* Video evidence link: Please paste a link to a viewable uploaded link.
```

<!-- End of section -->
